_ = require('underscore');
fs = require('fs');
// importNetflix('/home/sven/downloads/nf_prize_dataset/');

function importNetflix(folder, maxImport) {
  if (folder.substr(-1) != '/') folder += '/';
  var movieCount = 0, ratingCount = 0, movieMap = {};

  console.log('\nImporting movies...');
  _.each(read(folder+'movie_titles.txt'), function(values) {
    // MovieID,YearOfRelease,Title
    insertMovie(values[0], values[2], values[1]);
    movieCount++;
    movieMap[values[0]] = values[2];
  });


  console.log('\nImporting ratings...');
  _.each(fileList(folder + 'training_set'), function(file, index) {
    if (maxImport && index > maxImport) return false; // skip
    var movieId, localRatingCount = 0;
    _.each(read(folder + 'training_set/' + file), function(values) {
      if (!movieId) {
        // first line "MovieID:"
        movieId = values[0].substr(0, values[0].length-1);
      }
      // "CustomerID,Rating in 1-5 stars,Date"
      var rating = ((values[1] == 4) || ( values[1] == 5) ? 1 : -1),
          date = new Date(values[2]); // YYYY-MM-DD
      insertRating(values[0], movieId, rating, date);
      ratingCount++;
      localRatingCount++;
    });
    console.log(index + ": " + file + ": " + localRatingCount + " ratings imported for " + movieId + ": " + movieMap[movieId]);
  });

  console.log('\nImporting finished!');
  console.log('Imported movies', movieCount);
  console.log('Imported ratings', ratingCount);
}

function insertMovie(id, title, year) {
  DB.Movies.update({ _id: id },
                   { $set: { title: title, release: year } },
                   { upsert: true });
}
function insertRating(deviceId, movieId, rating, date) {
  Rating.setRating(deviceId, movieId, rating, date);
}

function fileList(path) { return fs.readdirSync(path); }
function read(file) {
  var data = fs.readFileSync(file, { encoding: "utf-8", flag: "r"});
  return _.map((data || "").split('\n'), function(line) {
    return line.split(',');
  });
}

module.exports.import = importNetflix;
