Config = require('./config.json')[process.env.NODE_ENV || 'development'];
Constants = require('./api/util/constants.js');
DB = require('./api/db/db.js');
// importing auth & server stuff just to use rating.js
Restify = require('restify');
Server = Restify.createServer();
Auth = require('./api/auth.js');
Rating = require('./api/rating.js');
Importer = require('./import.js');

var folder    = process.argv[2], // 0 = node, 1 = .../runImporter.js
    maxImport = process.argv[3];

console.log("Importing ratings of " + maxImport + " movies from " + folder);


setTimeout(function() { // "stupid" way to wait for DB to load
  Importer.import(folder, maxImport);
  //setTimeout(function() { process.exit(); }, 1000);
}, 1000);
