// imports
Config = require('../config.json')[process.env.NODE_ENV];
Constants = require('./util/constants.js');
Util = require('./util/util.js');
DB = require('./db/db.js');
_ = require('underscore');
Restify = require('restify');
Agent = require('superagent');

Server = Restify.createServer();

Server.use(Restify.bodyParser());
Server.use(Restify.CORS());
Server.use(Restify.queryParser());

// user scripts
Auth = require('./auth.js');
TmdbApi = require('./tmdb-api.js');
Device = require('./device.js');
Movie = require('./movie.js');
Rating = require('./rating.js');
Watchlist = require('./watchlist.js');
Suggest = require('./suggest.js');

// server start up
Server.listen(5003, function () {
    console.log('Cinematch API started on %s', Server.url);
});

statusOK = function (res) {
    respond(res, {status: 'OK'})
};

proceed = function (res, payload) {
    respond(res, payload);
};

reject = function (res, errorCode, errorMessage) {
    respond(res, undefined, {
        code: errorCode,
        message: errorMessage
    });
};

var respond = function (res, payload, err) {
    res.send(
        err ?
        {status: 'FAILURE', error: err} :
        {status: 'OK', payload: payload}
    );
};

redirect = function (res, location) {
    res.header('Location', location);
    res.send(302);
};
