Server.get('/watchlist', Auth.routeAuth(function (device, req, res, next) {
    DB.Watchlist.find({deviceId: device.deviceId}, {_id: 0, deviceId: 0}).toArray(function (err, data) {
        proceed(res, data);
    });
}));

Server.get('/watchlist/add', Auth.routeAuth(function (device, req, res, next) {
    DB.Watchlist.insert({deviceId: device.deviceId, movieId: req.params.movieId, addedOn: new Date()}, function (err) {
        if (err) throw err;

        statusOK(res);
    });
}));

Server.get('/watchlist/remove', Auth.routeAuth(function (device, req, res, next) {
    DB.Watchlist.remove({deviceId: device.deviceId, movieId: req.params.movieId}, function (err) {
        if (err) throw err;

        statusOK(res);
    });
}));