Server.get('/ratings/like', Auth.routeAuth(function (device, req, res, next) {
    setRatingStatusOk(res, device.deviceId, req.params.movieId, 1);
}));

Server.get('/ratings/dislike', Auth.routeAuth(function (device, req, res, next) {
    setRatingStatusOk(res, device.deviceId, req.params.movieId, -1);
}));

Server.get('/ratings', Auth.routeAuth(function (device, req, res, next) {
    DB.Ratings.find({deviceId: device.deviceId}, {_id: 0, deviceId: 0}).toArray(function (err, data) {
        proceed(res, data);
    });
}));

var setRating = function (deviceId, movieId, rating, date, callback) {
      var id = deviceId+movieId;
      date = date || new Date(); // importer will pass date in
      DB.Ratings.update(
          { _id: id },
          { $set: {
            rating: rating,
            addedOn: date,
            deviceId: deviceId,
            movieId: movieId } },
          { upsert: true },
          function (err) {
              if (err) throw err;
              if (callback) callback();

              // denormalization
              DB.DeviceRatings.update(
                  { _id: deviceId },
                  { $push: (rating == 1 ? { up: movieId } : { down: movieId }) },
                  { upsert: true },
                  function(error) { if (error) throw error });
          }
      );
    },
    setRatingStatusOk = function(res, deviceId, movieId, rating) {
      setRating(deviceId, movieId, rating, undefined, function() {
        statusOK(res); });
    }

module.exports.setRating = setRating
