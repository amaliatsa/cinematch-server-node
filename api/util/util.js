var Util = require('util');
var Crypto = require('crypto');
var Traverse = require('traverse');

Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};

formatDateInstances = function (obj) {
    return Traverse(obj).forEach(function (x) {
        if (x && x.constructor && x.constructor.name == 'Date') {
            this.update(x.getTime());
        }
    });
};

String.prototype.formatAmount = function () {
    return (this.replace(/[,\. ]/g, '') / 100).toFixed(2);
};

String.prototype.normalizeAccountNumber = function () {
    return this.replace(/\D/g, '');
};

module.exports = {
    getRandomNumberBetween: function (from, to) {
        return Math.floor(Math.random() * to) + 1;
    },
    format: Util.format,
    score: function (distance) {
        return Math.max(0, 10000 - distance);
    },
    generateRandomToken: function () {
        return Crypto.createHash('md5').update(Math.random().toString()).digest('hex').substring(0, 24);
    },
    formatDateInstances: formatDateInstances,
    normalizeAccounts: function (original) {
        return _.each(original, function (dirty) {
            dirty.accountNumber = dirty.accountNumber.normalizeAccountNumber();
            if (!_.isNumber(dirty.amount)) {
                dirty.amount = dirty.amount.formatAmount();
            }
        });
    },
    normalizeTransactions: function (original) {
        return _.each(original, function (dirty) {
            dirty.amount = dirty.amount.formatAmount();
        });
    },
    encrypt: function (text, key) {
        var cipher = Crypto.createCipher('aes-256-cbc', key);
        var encrypted = cipher.update(text, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    },
    decrypt: function (text, key) {
        var decipher = Crypto.createDecipher('aes-256-cbc', key);
        var decrypted = decipher.update(text, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    }
};