var getFromApi = function (endpoint, query, callback) {
    Agent.get('http://api.themoviedb.org/3/' + endpoint)
        .query({'api_key': Config.tmdbApiKey})
        .query(query)
        .set('Accept', 'application/json')
        .end(function (err, response) {
            if (err) throw err;
            var data = eval("(" + response.text + ")");
            _.each(data.results, function (entry) {
                entry.backdrop_path = TmdbApi.getImageUrl(entry.backdrop_path, 'large');
                entry.poster_path = TmdbApi.getImageUrl(entry.poster_path, 'large');
            });
            callback(data);
        });
};

var supportedWidths = {
    'default': 'w300',
    xxlarge: 'w1000',
    xlarge: 'w500',
    large: 'w300',
    medium: 'v185',
    small: 'w150'
};

module.exports = {
    searchMovie: function (query, callback) {
        getFromApi('search/movie', query, callback);
    },
    listPopular: function (query, callback) {
        getFromApi('movie/popular', query, callback);
    },
    getImageUrl: function (path, size) {
        return 'https://image.tmdb.org/t/p/' + supportedWidths[size || 'default'] + path;
    }
};