Server.get('/auth/token', function (req, res, next) {
    var token = Util.generateRandomToken();

    if (req.params.deviceId) {
        DB.Devices.update(
            {
                deviceId: req.params.deviceId
            },
            {
                $setOnInsert: {
                    addedOn: new Date()
                },
                $set: {
                    token: token
                }
            },
            {
                upsert: true
            },
            function (err) {
                if (err) throw err;

                proceed(res, {
                    token: token
                });
            }
        )
    }
    else {
        throw "Provide the deviceId parameter in order to receive a token"
    }
});

module.exports = {
    routeAuth: function (callback) {

        function checkTokenAndExecuteCallback(req, res, next) {
            DB.Devices.findOne({token: req.params.token}, function (err, device) {
                if (err) throw err;
                if (device) {
                    if (callback) {
                        callback(device, req, res, next);
                    }
                }
                else {
                    throw Util.format('Device associated not found for the %s token', req.params.token);
                }
            });
        }

        return checkTokenAndExecuteCallback;
    }
};