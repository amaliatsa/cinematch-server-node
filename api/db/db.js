var Mongo = require('mongodb');
var MongoClient = Mongo.MongoClient;

MongoClient.connect('mongodb://' + Config.dbHost + ':27017/cinematch', function (err, db) {
    if (err) throw err;

    _.extend(module.exports, {
        db: db,
        Movies: db.collection('movies'),
        Ratings: db.collection('ratings'),
        DeviceRatings: db.collection('deviceRatings'),
        Devices: db.collection('devices'),
        Watchlist: db.collection('watchlist')
    });
});
