// TODO either make everything async or use mongo-sync

Server.get('/suggest', Auth.routeAuth(function (device, req, res, next) {
    proceed(res, suggest(device.deviceId));
}));

function suggest(deviceId) {
    var myRatings = getRatings(deviceId);
    var myMovies = myRatings.up;
    var otherMovies = getNeighborsMovies(myRatings);
    return _.chain(otherMovies).without(myMovies).shuffle().first(10).value();
}

function getRatings(deviceId) {
  return DB.DeviceRatings.findOne({ deviceId: deviceId });
}

function getNeighborsMovies(ratings) {
  // how many neighbors are needed, % of ratings that need to match for first try
  var minNeighbors = 1, maxNeighbors = 10, startRequired = 1 / 8, maxCycles = 5,
      baseDown = _.shuffle(ratings.down),
      baseUp = _.shuffle(ratings.up),
      cycles = 0,
      required = startRequired,
      frame = [0, 1], // current search frame, min and max
      neighbor,
      pick = function(list) {
        return _.first(list, Math.round(list.length * required)); }

  while (true) {
    // pick a few up and down ratings, count users with the same ratings
    var query = { up: pick(baseUp), down: pick(baseDown) },
        count = DB.DeviceRatings.count(query);
    if ((++cycle > maxCycles) ||
        ((count >= minNeighbors) && (count <= minNeighbors)))
      // found neighborhood! Get ratings, extract up-voted movieIds
      return _.chain(DB.DeviceRatings.find(query))
              .pluck('up').flatten().uniq().compact().value();
    if (count < minRequired)
         frame[1] = required; // move frame left > less ratings > more neighbors
    else frame[0] = required; // move frame right > more ratings > less neighbors
    required = (frame[0] + frame[1]) / 2;
  }

  return []; // throw error?
}

module.exports = {
    suggestMovies: suggest
};
