Server.get('/movies/search', function (req, res, next) {
    TmdbApi.searchMovie({
        query: req.params.title,
        page: req.params.page || 1
    }, function (data) {
        proceed(res, data);
    });
});

Server.get('/movies/popular', function (req, res) {
    TmdbApi.listPopular({page: req.params.page || 1}, function (data) {
        proceed(res, data);
    })
});